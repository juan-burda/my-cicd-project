import { defineConfig } from "cypress";

export default defineConfig({
    viewportHeight: 1080,
    viewportWidth: 1920,
    video: true,
    screenshotOnRunFailure: true,
    e2e: {
        setupNodeEvents(on, config) {},
    },
});